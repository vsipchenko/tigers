from django.db import models

from tigers.general.utils import generate_filename


class News(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=generate_filename)

    def __str__(self):
        return str(self.title)
