from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

from tigers.api.general_views import api_root, home_content_view, \
    contacts_view, \
    NewsListView, AlbumViewSet, homepage_sliders_view

router = SimpleRouter()
router.register(r'albums', AlbumViewSet, base_name='albums')

urlpatterns = [
    url(r'^$', api_root, name='root'),
    url(r'^', include(router.urls)),
    url(r'^home/$', home_content_view, name='home'),
    url(r'^contacts/$', contacts_view, name='contacts'),
    url(r'^news/$', NewsListView.as_view(), name='news'),
    url(r'^sliders/$', homepage_sliders_view, name='sliders-list'),

]
