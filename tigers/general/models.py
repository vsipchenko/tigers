from django.db import models


class HomeContent(models.Model):
    welcome_text = models.TextField()
    welcome_title = models.CharField(max_length=255)
    team_introduction_text = models.TextField()
    team_introduction_title = models.CharField(max_length=255)
    about_ultimate_short_note = models.TextField()

    def __str__(self):
        return str('Content of homepage')


class AboutUltimateText(models.Model):
    text = models.TextField()
    title = models.CharField(max_length=255)


class Contacts(models.Model):
    first_phone = models.CharField(max_length=50)
    second_phone = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    vk_link = models.CharField(max_length=50)
    fb_link = models.CharField(max_length=50)
    other = models.CharField(max_length=255)

    def __str__(self):
        return str('Contact_info')
