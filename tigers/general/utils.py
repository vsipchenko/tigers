import os
import re
import uuid

from django.utils import timezone


def slugify_camelcase(string, sep='-'):
    repl = r'\1{}\2'.format(sep)
    s1 = re.sub('(.)([A-Z][a-z]+)', repl, string)
    return re.sub('([a-z0-9])([A-Z])', repl, s1).lower()


def generate_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    model_name = slugify_camelcase(instance._meta.model.__name__, '_')
    strftime = timezone.datetime.now().strftime('%Y/%m/%d')
    hex = uuid.uuid4().hex
    return '{}s/{}/{}{}'.format(model_name, strftime, hex, ext)
