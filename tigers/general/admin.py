from django.contrib import admin

from tigers.general.models import HomeContent, AboutUltimateText, Contacts


class HomeContentAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        items = HomeContent.objects.all()
        if items.count() >= 1:
            return False
        else:
            return True


class ContactsAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        items = Contacts.objects.all()
        if items.count() >= 1:
            return False
        else:
            return True


admin.site.register(HomeContent, HomeContentAdmin)
admin.site.register(AboutUltimateText)
admin.site.register(Contacts, ContactsAdmin)
