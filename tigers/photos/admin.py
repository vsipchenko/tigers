from django.contrib import admin

from tigers.photos.models import Photo, Album, HomePageSliders

admin.site.register(Photo)
admin.site.register(Album)
admin.site.register(HomePageSliders)